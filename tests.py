import sys
sys.path.append('/Users/rodrigo/src/pyjamas_dev')

from pyjamas.pjscore import PyJAMAS
from pyjamas.rplugins.base import PJSPluginABC
from pyjamas.dialogs.textdialog import TextDialog
from recoil_velocity import PJSPlugin

a = PyJAMAS()
a.plugins.cbInstallPlugin('/Users/rodrigo/src/pyjamas_plugins/recoil_velocity/recoil_velocity.py')



parameters = {
    'folders': ['/Users/rodrigo/Images/mesectoderm/ectoderm_ap/', '/Users/rodrigo/Images/mesectoderm/mesectoderm_ap/', '/Users/rodrigo/Images/mesectoderm/mesectoderm_dv/'],
    'analyze_flag': True,
    'analysis_filename_appendix': '_analysis',
    'analysis_extension': '.csv',
    'save_results': True,
    'results_folder': '/Users/rodrigo/Images/mesectoderm/results/',
    'script_filename_appendix': '_analysis_script',
    't_res': 4,
    'xy_res': 16 / (100 * 1.5),
    'num_slices_fit': 9,
    'r_min': 0.8,
    'index_time_zero': 1,
    'plot_flag': False,
    'names': ['ectoderm AP', 'mesectoderm AP', 'mesectoderm DV'],
    'err_style_value': 'band',
    'ablation_time': 6*15/100*2+67/100,
}

plugin_index = [ind for ind, x in enumerate(
    a.plugin_list) if x.name() == 'Recoil velocity'][-1]
b = a.plugin_list[plugin_index]

b._read_parameters(parameters)
b._measure_data(parameters)
all_data = b._combine_measured_data()
data_path: str = b._save_data(all_data, parameters)
b._save_notebook(parameters, data_path)
