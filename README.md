[![GPLv3 License](https://img.shields.io/badge/License-GPL%20v3-yellow.svg)](https://opensource.org/licenses/GPL-3.0)

# Recoil velocity (a [**PyJAMAS**](https://bitbucket.org/rfg_lab/pyjamas/src/master/) plugin)

## Installation
This plugin requires [**PyJAMAS**](https://bitbucket.org/rfg_lab/pyjamas/src/master/) version 2023.12.0 or a more recent one.

Download this repository. Run [**PyJAMAS**](https://bitbucket.org/rfg_lab/pyjamas/src/master/) and select **Plugins**>**Install plugin ...**. Navigate to the location of **recoil_velocity.py** and select the file. The plugin should be available under the **Plugins** menu.

Alternatively, you  may move the entire folder containing  **recoil_velocity.py** into the *plugins* folder under the [**PyJAMAS**](https://bitbucket.org/rfg_lab/pyjamas/src/master/) installation that you are using. When you restart [**PyJAMAS**](https://bitbucket.org/rfg_lab/pyjamas/src/master/), the plugin should be available under the **Plugins** menu. 

## Use
To begin, annotate the images: add two fiducials at  the ends  of the severed structure. The fiducial ids should correspond across time points: 

![Fiducial annotations](./docs/fiducial_annotations.gif)

For recoil velocity analysis, the images acquired immediately before and after ablation should be annotated. For viscoelasticity analysis (by fitting a Kelvin-Voigt model), please annotate enough time points for the distance between the fiducials to approach its maximum, asymptotic value. Do not include time points during a potential wound healing response in which the distance between fiducials decreases: the Kelvin-Voigt model will not fit this well.

Organize your images in as many folders as experimental groups. Within each folder, there should be a subfolder per image containing the image and the corresponding fiducial annotations. For example, for the laser cut analysis in **(Yu, Balaghi, Erdemci-Tandogan et al., *Cells Dev.*, 2021)**, with three different experimental groups (ectoderm_ap, mesectoderm_ap and mesectoderm_dv; note that images were acquired in colour, and thus there are two images per folder, one image per channel):

![Image organization](./docs/image_organization.png)

When you run the plugin, a simple user interface appears displaying all the parameters for the plugin and the corresponding values in text format.

![Input dialog](./docs/input_dialog.png)

Modify the parameters as necessary:

**'folders'**:  list with full paths to folders containing data subfolders; there should be one folder in the list for each of the experimental groups.

**'names'**: list containing the experimental group names; theres should be one element per group.

**'analyze_flag'**: True if data must be analyzed, False to read previous analysis results.

**'analysis_filename_appendix'**: a string with the appendix to add to the name of the file that stores the analysis results for a given image (the rest of the name will match the annotation file name).

**'save_results'**: True to save results to disk, False otherwise.

**'results_folder'**: path to the folder where the combined analysis results will be stored.

**'t_res'**: temporal resolution in seconds.

**'ablation_time'**: time necessary to ablate and acquire the first image after ablation, in seconds.

**'xy_res'**: spatial resolution in microns.

**'index_time_zero'**: index of the last time point before ablation (e.g. if two images were acquired before ablation, this should be 1).

**'num_slices_fit'**: number of time points after **index_time_zero** to use for Kelvin-Voigt fitting. Set to *None* if all available time points should be used.

**'r_min'**: minimum correlation coefficient attained by Kelvin-Voigt fitting to consider a good fit.

**'plot_flag'**: True to generate and display plots, False otherwise.

**'err_style_value'**: select the type of error display in plots: one of 'band' or 'bars'.

**'plot_style_value'**: plot style for initial recoil velocity and Kelvin-Voigt fitting parameters; 'box' for a box-whisker plot, 'violin' for a violin plot.

**'line_width'**: width of the lines in plots in pixels.

Select "Cool!" to proceed with the analysis. The plugin will run through all the selected file groups. The analysis can  be visualized using the Jupyter notebook saved in the *results_folder*:

![Run analysis](./docs/run_analysis.gif)



