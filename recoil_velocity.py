import sys
from datetime import datetime
import os
from typing import Dict, List, Optional, Tuple
import warnings

import matplotlib.pyplot as plt
import nbformat as nbf
from nbformat.notebooknode import NotebookNode
import numpy
numpy.seterr(over='raise')  # Catch overflow errors.

import pandas as pd
from pandas.core.frame import DataFrame
from PyQt6 import QtCore, QtWidgets
from pandas.core.groupby.groupby import GroupByPlot
from scipy.optimize import leastsq
import seaborn as sns

from pyjamas.pjscore import PyJAMAS
from pyjamas.rplugins.base import PJSPluginABC
from pyjamas.dialogs.texteditdialog import TextEditDialog
from pyjamas.rutils import RUtils


class PJSPlugin(PJSPluginABC):
    BATCH_MEASURE_SCRIPT: str = f"import sys\nsys.path.extend(['{PyJAMAS.folder.rstrip('/pyjamas')}'])\nfrom pyjamas.pjscore import PyJAMAS\na = PyJAMAS()\na.plugin_list[[i for i, aplugin in enumerate(a.plugin_list) if aplugin.name()=='Recoil velocity' ][0]].run(parameters)"

    DIRS_BM: List[str] = ['', '']
    ANALYZE_FLAG_BM: bool = True
    ANALYSIS_FILENAME_APPENDIX_BM: str = '_analysis'
    ANALYSIS_EXTENSION_BM: str = '.csv'
    # Save the analysis script in each folder in which an analysis flag is saved.
    SAVE_RESULTS: bool = True
    RESULTS_FOLDER: str = '.'
    SCRIPT_EXTENSION_BM = '.py'
    SCRIPT_FILENAME_APPENDIX_BM: str = '_analysis_script' + PyJAMAS.notebook_extension
    INTENSITY_FLAG_BM: bool = True  # Run intensity section of the analysis/plots?
    IMAGE_EXTENSION_BM: str = '.tif'
    T_RES_BM: float = 4  # Time resolution in seconds.
    XY_RES_BM: float = 16 / (60 * 1.5)  # Spatial resolution in microns.
    INDEX_TIME_ZERO_BM: int = 1  # index = 0 would mean that the last time point before ablation is the first one in the movie.
    NUM_SLICES_FIT: Optional[int] = None  # How many time points after ablation to use for fitting. None for all available time points.
    R_MIN: float = 0.0  # Minimum correlation to consider fitting results usable.
    PLOT_FLAG_BM: bool = False  # Generate and display plots.
    GROUP_LABELS_BM: List[str] = ['group 1', 'group 2']
    ERR_STYLE_VALUE_BM: str = 'band'  # 'band' or 'bars'
    PLOT_STYLE_VALUE_BM: str = 'box'  # 'box' or 'violin'
    LINE_WIDTH_BM: int = 3

    # Read all data and compile into DataFrames.
    COMPILE_DATA_FLAG_BM: bool = True
    NTP_BM: int = 308

    ABS_MEASUREMENTS = ['distance (\u03BCm)']
    BOX_PLOT_MEASUREMENTS: List[str] = ['relaxation time (s) Kelvin-Voigt model', 'stress-elasticity (\u03BCm) Kelvin-Voigt model', 'relaxation time KV element (s) 3-parameter fluid model',  'relaxation time dashpot (s) 3-parameter fluid model', 'stress-elasticity (\u03BCm) 3-parameter fluid model', 'initial velocity (\u03BCm/s)']
    CORRELATION_MEASUREMENTS: List[str]  = ['correlation data vs. Kelvin-Voigt model', 'correlation data vs. 3-parameter fluid model']
    COLUMN_LIST_BM: List[str] = ['experimental group', 'experiment index', 'file name', 'time (s)'] + ABS_MEASUREMENTS + BOX_PLOT_MEASUREMENTS+CORRELATION_MEASUREMENTS

    # time in seconds necessary to ablate and acquire the first image after ablation
    ABLATION_TIME: float = 5

    def name(self) -> str:
        return 'Recoil velocity'

    def run(self, parameters: dict) -> bool:
        continue_flag: bool = True

        self._read_parameters(parameters)

        ui = TextEditDialog(self._param_str(), 'Recoil velocity analysis')
        ui.show()

        continue_flag = ui.result()

        if continue_flag:
            try:
                parameters = eval(ui.parameters())
            except SyntaxError as err:
                print(f"Syntax error: {err}")
                return False
        if continue_flag:
            return self.batch_measure(parameters)

        else:
            return False

    def batch_measure(self, parameters: dict) -> bool:
        self._read_parameters(parameters)

        if self.analyze_flag:
            self._measure_data(parameters)

        if self.compile_data_flag:
            all_data = self._combine_measured_data()

            if self.plot_flag:
                self._plot_data(all_data)

            if self.save_results:
                data_path: str = self._save_data(all_data, parameters)
                self._save_notebook(parameters, data_path)

        self.pjs.statusbar.showMessage(
            f"Analysis results stored in {parameters.get('results_folder')}.")

        return True

    def _param_str(self) -> str:
        param_str: str = f"{{\n\t'folders': {self.folders},  # list with full paths to folders containing data subfolders.\n\t'names': {self.group_labels},  # name for experimental groups.\n\t'analyze_flag': {self.analyze_flag},  # True if data must be analyzed, False to read previous analysis results.\n\t'analysis_filename_appendix': '{self.analysis_filename_appendix}',  # appendix to add to each file name to create a new file storing the analysis results.\n\t'save_results': {self.save_results},  # True to save results to disk, False otherwise.\n\t'results_folder': '{self.results_folder}',  # path to folder where combined results should be stored.\n\t't_res': {self.t_res},  # temporal resolution in seconds.\n\t'ablation_time': {self.ablation_time},  # time necessary to ablate and acquire the first image after ablation in seconds.\n\t'xy_res': {self.xy_res},  # spatial resolution in microns.\n\t'index_time_zero': {self.index_time_zero},  # index of the last time point before ablation (e.g. if two images were acquired before ablation, this should be 1).\n\t'num_slices_fit': {self.num_slices_fit},  # Number of slices after index_time_zero to use for fitting. None if all available slices.\n\t'r_min': {self.r_min},  # Minimum correlation coefficient between data distance data and Kelvin-Voigt model to consider a good fit.\n\t'plot_flag': {self.plot_flag},  # True to generate and display plots, False otherwise.\n\t'err_style_value': '{self.err_style_value}',  # 'band' or 'bars'.\n\t'plot_style_value': '{self.plot_style_value}',  # 'box' for a box-whisker plot, 'violin' for a violin plot.\n\t'line_width': {self.line_width},  # width in pixels of the lines in plots.\n}}"

        return param_str

    def _read_parameters(self, parameters: dict) -> bool:
        if parameters is None or parameters is False:
            parameters = {}

        self.folders: List[str] = parameters.get('folders', PJSPlugin.DIRS_BM)
        PJSPlugin.DIRS_BM = self.folders
        self.group_labels: List[str] = parameters.get('names', PJSPlugin.GROUP_LABELS_BM)
        PJSPlugin.GROUP_LABELS_BM = self.group_labels

        self.all_annotations: List = []
        self.all_images: List = []
        self.all_labels: List = []

        for ii, afolder in enumerate(self.folders):
            if afolder == '' or not os.path.isdir(afolder):
                self.pjs.statusbar.showMessage(f'{afolder} is not a folder.')
                continue
            new_annotations: List[str] = RUtils.extract_file_paths(
                afolder, [PyJAMAS.data_extension, PyJAMAS.matlab_extension])
            self.all_annotations.append(new_annotations)
            self.all_labels.append([self.group_labels[ii] for x in range(len(new_annotations))])

            new_images: List[str] = RUtils.extract_file_paths(afolder, PyJAMAS.image_extensions)
            self.all_images.append([x for x in new_images])

        self.analyze_flag: bool = parameters.get(
            'analyze_flag', PJSPlugin.ANALYZE_FLAG_BM)
        PJSPlugin.ANALYZE_FLAG_BM = self.analyze_flag
        self.analysis_filename_appendix: str = parameters.get(
            'analysis_filename_appendix', PJSPlugin.ANALYSIS_FILENAME_APPENDIX_BM)
        PJSPlugin.ANALYSIS_FILENAME_APPENDIX_BM = self.analysis_filename_appendix
        self.analysis_extension: str = parameters.get(
            'analysis_extension', PJSPlugin.ANALYSIS_EXTENSION_BM)
        PJSPlugin.ANALYSIS_EXTENSION_BM = self.analysis_extension
        # Save the analysis script in each folder in which an analysis flag is saved.
        self.save_results: bool = parameters.get(
            'save_results', PJSPlugin.SAVE_RESULTS)
        PJSPlugin.SAVE_RESULTS =  self.save_results
        # Save the analysis script in each folder in which an analysis flag is saved.
        self.results_folder: str = parameters.get(
            'results_folder', PJSPlugin.RESULTS_FOLDER)
        PJSPlugin.RESULTS_FOLDER = self.results_folder
        self.script_filename_appendix: str = parameters.get(
            'script_filename_appendix', PJSPlugin.SCRIPT_FILENAME_APPENDIX_BM)
        PJSPlugin.SCRIPT_FILENAME_APPENDIX_BM = self.script_filename_appendix
        # Time resolution in seconds.
        self.t_res: float = parameters.get('t_res', PJSPlugin.T_RES_BM)
        PJSPlugin.T_RES_BM = self.t_res
        # Spatial resolution in microns.
        self.xy_res: float = parameters.get('xy_res', PJSPlugin.XY_RES_BM)
        PJSPlugin.XY_RES_BM = self.xy_res
        # Points at the time point before ablation  (e.g. == 1 if two images before ablation).
        self.index_time_zero: int = parameters.get(
            'index_time_zero', PJSPlugin.INDEX_TIME_ZERO_BM)
        PJSPlugin.INDEX_TIME_ZERO_BM = self.index_time_zero
        # Number of slices to use for fitting. Counting from index_time_zero.
        self.num_slices_fit: Optional[int] = parameters.get('num_slices_fit', PJSPlugin.NUM_SLICES_FIT)
        PJSPlugin.NUM_SLICES_FIT = self.num_slices_fit
        # Minimum decent correlation.
        self.r_min: float = parameters.get('r_min', PJSPlugin.R_MIN)
        PJSPlugin.R_MIN = self.r_min

        # Generate and display plots.
        self.plot_flag: bool = parameters.get(
            'plot_flag', PJSPlugin.PLOT_FLAG_BM)
        PJSPlugin.PLOT_FLAG_BM = self.plot_flag
        self.err_style_value: str = parameters.get(
            'err_style_value', PJSPlugin.ERR_STYLE_VALUE_BM)
        PJSPlugin.ERR_STYLE_VALUE_BM = self.err_style_value
        self.plot_style_value: str = parameters.get(
            'plot_style_value', PJSPlugin.PLOT_STYLE_VALUE_BM)
        PJSPlugin.PLOT_STYLE_VALUE_BM = self.plot_style_value
        self.line_width: int = parameters.get('line_width', PJSPlugin.LINE_WIDTH_BM)
        PJSPlugin.LINE_WIDTH_BM = self.line_width
        # Read all data and compile into DataFrames.
        self.compile_data_flag: bool = PJSPlugin.COMPILE_DATA_FLAG_BM
        self.ntp: int = PJSPlugin.NTP_BM
        self.column_list: List[str] = PJSPlugin.COLUMN_LIST_BM
        self.ablation_time: float = parameters.get(
            'ablation_time', PJSPlugin.ABLATION_TIME)
        PJSPlugin.ABLATION_TIME = self.ablation_time
        self.t = numpy.nan * numpy.zeros(self.ntp)
        self.t[0:self.index_time_zero +
               1] = numpy.arange(self.index_time_zero+1) - self.index_time_zero
        self.t[self.index_time_zero +
               1:] = numpy.arange(self.ntp-self.index_time_zero-1)
        self.t *= self.t_res
        self.t[self.index_time_zero+1:] += self.ablation_time

        return True

    def _measure_data(self, parameters: dict) -> bool:
        # Store currently open image before we open others.
        prev_image: numpy.ndarray = self.pjs.slices.copy()
        prev_wd: str = os.getcwd()

        for theannotations, theimages in zip(self.all_annotations, self.all_images):
            for ii, annotations_file in enumerate(theannotations):
                print(
                    f"Analyzing movie {ii + 1}/{len(theannotations)} ... ", end="")

                file_path, full_file_name = os.path.split(annotations_file)
                file_name, ext = os.path.splitext(full_file_name)

                # Create analysis file path.
                full_analysis_file_name = os.path.join(
                    file_path, file_name + self.analysis_filename_appendix + self.analysis_extension)
                full_script_file_name = os.path.join(
                    file_path, file_name + self.script_filename_appendix)

                # Load annotation file (and image if intensity measurements are happening).
                if os.path.isfile(annotations_file) and ext == PyJAMAS.data_extension:
                    self.pjs.io.cbLoadAnnotations(
                        [annotations_file], image_file=theimages[ii])
                elif os.path.isfile(annotations_file) and ext == PyJAMAS.matlab_extension:
                    self.pjs.io.cbImportSIESTAAnnotations(
                        [annotations_file], image_file=theimages[ii])
                else:
                    continue

                # Find indexes of first and last slice with fiducials.
                first: int = 0

                for slice in self.pjs.fiducials:
                    if slice == []:
                        first += 1
                    else:
                        break

                last: int = len(self.pjs.fiducials) - 1

                for slice in self.pjs.fiducials[-1::-1]:
                    if slice == []:
                        last -= 1
                    else:
                        break

                # self.measure_movie_recoil measures the recoil velocity and models the results
                self.measure_movie_recoil(first, last, full_analysis_file_name)

                # Analysis script saved here so that we know where each analysis result file came from.
                if self.save_results:
                    with open(full_script_file_name, "w") as f:
                        print(
                            f"parameters = {str(parameters)}\n{PJSPlugin.BATCH_MEASURE_SCRIPT}", file=f)

                print("done!")

        # Restore GUI to its state before beginning batch analysis.
        self.pjs.io.cbLoadArray(prev_image)
        self.pjs.options.cbSetCWD(prev_wd)

        return True

    def measure_movie_recoil(self, first_index: int = -1, last_index: int = -1,  filename: Optional[str] = None) -> Optional[Dict]:
        # All the parameters you need are read in _read_parameters (xy_res, t_res, index_time_zero)
        # Implement cut_analysis here.

        theresults: Dict = {}

        # Create and open dialog for measuring polygons.
        if filename == '' or filename is False or filename is None or \
                first_index is False or first_index is None or last_index is False or last_index is None:
            return theresults

        if first_index > last_index:
            first_index, last_index = last_index, first_index

        filename = RUtils.set_extension(filename, '.csv')

        theslicenumbers = numpy.arange(first_index, last_index+1)

        theresults = self._do_measure(theslicenumbers)

        if  theresults is None:
            print(
                f"The annotation file at {os.path.dirname(filename)} contains an abnormal number of fiducials and has not been used.")
            return None

        # If a file name was entered, save the data.
        if filename != '':
            theresults.to_csv(filename)

        else:
            with pd.option_context('display.max_columns', sys.maxsize):
                print(theresults)

        return theresults

    def _do_measure(self, slices: numpy.ndarray) -> Optional[pd.DataFrame]:
        # Returns a pandas DataFrame in which columns represent time points and rows correspond to ablation measurements.
        # :param slices: slice indexes -start at 0-.
        # :return: data frame with the measurements, or None if there aren't at least two fiducials in the indicated slices.

        row_names: List[str] = self.column_list[2:]

        rows: int = len(row_names)
        columns: int = slices.shape[0]

        measurement_df: pd.DataFrame = pd.DataFrame(
            numpy.nan * numpy.zeros((rows, columns)), columns=slices + 1, index=row_names)

        # For every slice ...
        for i in slices:
            thefiducials = self.pjs.fiducials[i]

            if len(thefiducials) < 2:
                return None

            p1: List[int] = thefiducials[0]
            p2: List[int] = thefiducials[1]

            # When indexing a DataFrame with loc, we use the row and column  names (i+1 is the name of the column, not an index).
            measurement_df.loc['distance (\u03BCm)', i+1] = numpy.linalg.norm(
                (p2[0]-p1[0], p2[1]-p1[1]), 2)*self.xy_res

        # Now fit the results.
        thedistances: numpy.ndarray = measurement_df.loc['distance (\u03BCm)', :].to_numpy()
        thedistances = (thedistances - thedistances[self.index_time_zero])
        thetimes = self.t[slices]

        end_fit:int = thedistances.shape[0]+1 if not self.num_slices_fit else self.index_time_zero+self.num_slices_fit+1

        x_kv: numpy.ndarray = None
        x_3param: numpy.ndarray = None
        r_kv: float = -1.
        r_3param: float = -1.

        try:
            x_kv, _ = leastsq(RUtils.residuals, (thedistances[-1], 1.0), args=(
                self.kelvin_voigt, thedistances[self.index_time_zero:end_fit], thetimes[self.index_time_zero:end_fit]))
        except FloatingPointError:
            r_kv = numpy.nan
            x_kv = numpy.asarray((numpy.nan, numpy.nan))
        else:
            r_kv = numpy.corrcoef(thedistances[self.index_time_zero:end_fit], self.kelvin_voigt(
                thetimes[self.index_time_zero:end_fit], x_kv))[0, 1]
        
        try:
            x_3param, _ = leastsq(RUtils.residuals, (thedistances[-1], 1.0, 1.0), args=(
                self.three_parameter_fluid, thedistances[self.index_time_zero:end_fit], thetimes[self.index_time_zero:end_fit]))
        except FloatingPointError:
            r_3param = numpy.nan
            x_3param = numpy.asarray((numpy.nan, numpy.nan, numpy.nan))
        else:
            r_3param = numpy.corrcoef(thedistances[self.index_time_zero:end_fit], self.three_parameter_fluid(
                thetimes[self.index_time_zero:end_fit], x_3param))[0, 1]   

        measurement_df.loc['correlation data vs. Kelvin-Voigt model', :] = r_kv
        measurement_df.loc['time (s)', :] = thetimes
        measurement_df.loc['stress-elasticity (\u03BCm) Kelvin-Voigt model', :] = x_kv[0] if r_kv > self.r_min and x_kv[1] >= 0. else numpy.nan  # relaxation time cannot be negative (e.g. distance decreases over time).
        measurement_df.loc['relaxation time (s) Kelvin-Voigt model',
                           :] = x_kv[1] if r_kv > self.r_min and x_kv[1] >= 0. else numpy.nan  # relaxation times cannot be negative.
        measurement_df.loc['correlation data vs. 3-parameter fluid model', :] = r_3param
        measurement_df.loc['stress-elasticity (\u03BCm) 3-parameter fluid model', :] = x_3param[0] if r_3param > self.r_min and x_3param[1] >= 0. and x_3param[2] >= 0. else numpy.nan  # relaxation times cannot be negative.
        measurement_df.loc['relaxation time KV element (s) 3-parameter fluid model',
                           :] = x_3param[1] if r_3param > self.r_min and x_3param[1] >= 0. and x_3param[2] >= 0. else numpy.nan  # relaxation times cannot be negative.
        measurement_df.loc['relaxation time dashpot (s) 3-parameter fluid model',
                           :] = x_3param[2] if r_3param > self.r_min and x_3param[1] >= 0. and x_3param[2] >= 0. else numpy.nan  # relaxation times cannot be negative.

        measurement_df.loc['initial velocity (\u03BCm/s)',
                           :] = thedistances[self.index_time_zero+1] / self.ablation_time

        return measurement_df

    @classmethod
    def kelvin_voigt(cls, x: numpy.ndarray, coeffs: Tuple[float, float]) -> numpy.ndarray:
        return coeffs[0] * (1. - numpy.exp(- x / coeffs[1]))

    @classmethod
    def three_parameter_fluid(cls, x: numpy.ndarray, coeffs: Tuple[float, float, float]) -> numpy.ndarray:
        return coeffs[0] * (1. - numpy.exp(- x / coeffs[1]) + x / coeffs[2])

    def _combine_measured_data(self) -> pd.DataFrame:
        n_files_to_analyze = sum((len(alist) for alist in self.all_annotations))

        # all_data is the DataFrame in which all the data will be compiled.
        # It contains one row per folder (i.e. annotation file) and time point.
        # Each row contains multiple columns with the values for different metrics in different folders and time points.
        all_data: pd.DataFrame = pd.DataFrame(
            numpy.nan *
            numpy.zeros((n_files_to_analyze * self.ntp,
                         len(self.column_list))),
            columns=self.column_list)

        msr_df: Optional[DataFrame] = None
        min_ind: int = -1
        max_ind: int = -1

        for jj, folder_list in enumerate(self.all_annotations):
            for ii, annotations_file in enumerate(folder_list):
                file_path, full_file_name = os.path.split(annotations_file)
                file_name, ext = os.path.splitext(full_file_name)

                # Create analysis file path.
                full_analysis_file_name = os.path.join(file_path,
                                                    file_name + self.analysis_filename_appendix + self.analysis_extension)

                # Load annotation file (and image if intensity measurements are happening).            
                if os.path.isfile(full_analysis_file_name):
                    msr_df = pd.read_csv(full_analysis_file_name, index_col=0)
                else:
                    continue

                # min_ind and max_ind delimit the rows in all_data that correspond to the time points for the current folder.
                min_ind = max_ind+1
                # these are row names, not indices, so loc uses ALL OF THEM (not until max_ind-1). Thus, we need  to force the -1.
                max_ind = min_ind + self.ntp - 1

                # Determine both the experimental group and the experiment index within that group.
                all_data.loc[min_ind:max_ind,
                                'experimental group'] = self.all_labels[jj][ii]
                all_data.loc[min_ind:max_ind, 'experiment index'] = ii

                all_data.loc[min_ind:max_ind, 'time (s)'] = self.t

                all_data.loc[min_ind:max_ind, 'file name'] = full_analysis_file_name

                for themetric in self.ABS_MEASUREMENTS + self.BOX_PLOT_MEASUREMENTS + self. CORRELATION_MEASUREMENTS:
                    all_data.loc[min_ind:min_ind+msr_df.loc[themetric].size-1, themetric] = msr_df.loc[themetric].values

        return all_data

    def _plot_data(self, all_data: pd.DataFrame) -> bool:
        # We are going to plot A LOT. So we silence the warning that too many plots were opened.
        warnings.filterwarnings(
            "ignore", message="More than 20 figures have been opened.")

        self._setup_plots()

        # Mean plots comparing multiple groups.
        n_groups: int = len(numpy.unique(all_data['experimental group']))
        a_palette = sns.color_palette('bright',
                                      n_groups)  # The "bright" colour palette will cycle for more than 10 colours.

        # todo: outward pointing ticks, axes units.
        for a_measurement in self.ABS_MEASUREMENTS:
            plt.figure()
            ax = sns.lineplot(x='time (s)', y=a_measurement, hue='experimental group', data=all_data,
                              errorbar='se',
                              err_style=self.err_style_value, lw=self.line_width, palette=a_palette)
            ax.legend(frameon=False)

            if not numpy.isinf(ax.dataLim.ymax):
                ax.set_ylim([min(ax.dataLim.ymin - abs(.05 * ax.dataLim.ymin), 0),
                             ax.dataLim.ymax + abs(.05 * ax.dataLim.ymax)])

            sns.despine()
            plt.show()

        # Plot individual curves for each of the folders.
        # All in the same plot.
        for a_measurement in self.ABS_MEASUREMENTS:
            plt.figure()
            ax = sns.lineplot(x='time (s)', y=a_measurement, hue='experimental group', data=all_data, estimator=None,
                              units='experiment index', lw=self.line_width, palette=a_palette)
            if not numpy.isinf(ax.dataLim.ymax):
                ax.set_ylim([min(ax.dataLim.ymin - abs(.05 * ax.dataLim.ymin), 0),
                             ax.dataLim.ymax + abs(.05 * ax.dataLim.ymax)])
            handles, labels = ax.get_legend_handles_labels()
            ax.legend(handles=handles[0:], labels=labels[0:], frameon=False)

            sns.despine()
            plt.show()

            # Each group independently, color-coding based on experiment index within the group.
            for a_group in self.group_labels:
                thedata = all_data[all_data['experimental group'] == a_group]

                if numpy.array(thedata).size == 0:
                    continue

                n_exp: int = len(numpy.unique(
                    thedata['experiment index']).astype(int))

                plt.figure()
                ax = sns.lineplot(x='time (s)', y=a_measurement, hue='experiment index', data=thedata,
                                  estimator=None, units='experiment index', lw=self.line_width,
                                  palette=sns.color_palette("husl",
                                                            n_exp))  # draw evenly-spaced colors in a circular color space
                if not numpy.isinf(ax.dataLim.ymax):
                    ax.set_ylim([min(ax.dataLim.ymin - abs(.05 * ax.dataLim.ymin), 0),
                                 ax.dataLim.ymax + abs(.05 * ax.dataLim.ymax)])
                ax.legend([str(x) for x in range(n_exp)],
                          frameon=False, title=a_group)
                sns.despine()
                plt.show()

        # Box plots for metrics that summarize experiments with a number each (eg. area rate constant).
        # The rate constant is the same for all the time points of a given image, so we only take the first
        # value per image.
        # sort=False is important to preserve the order of the experimental groups, so that labels in the
        # plots below are correct.
        thedata = all_data.groupby(
            ['experimental group', 'experiment index'], as_index=False, sort=False).first()

        for a_measurement in self.BOX_PLOT_MEASUREMENTS:
            plt.figure()

            if self.plot_style_value == 'box':
                # whis is the proportion of the IQR past the low and high quartiles to extend the plot whiskers.
                # Points outside this range will be identified as outliers (and error bars will ignore them).
                ax = sns.boxplot(x='experimental group', y=a_measurement, data=thedata, hue='experimental group',
                                 dodge=False,
                                 whis=100000, palette=a_palette)
            elif self.plot_style_value == 'violin':
                ax = sns.violinplot(x='experimental group', y=a_measurement, data=thedata, hue='experimental group',
                                    legend='brief', dodge=False)

            ax.legend(frameon=False)

            ax = sns.stripplot(x='experimental group', y=a_measurement, data=thedata, color='k', alpha=0.75, size=6,
                               dodge=False,
                               jitter=0.05)
            if not numpy.isinf(ax.dataLim.ymax):
                ax.set_ylim([min(ax.dataLim.ymin - abs(.05 * ax.dataLim.ymin), 0),
                             ax.dataLim.ymax + abs(.05 * ax.dataLim.ymax)])

            sns.despine()
            plt.show()

        return True

    def _setup_plots(self) -> bool:
        sns.set()
        sns.set_style("white")

        plt.rcParams['font.size'] = 14
        plt.rcParams['font.weight'] = 'bold'
        plt.rcParams['xtick.labelsize'] = 16
        plt.rcParams['ytick.labelsize'] = 16
        plt.rcParams['axes.labelsize'] = 18
        plt.rcParams['axes.labelweight'] = 'bold'
        plt.rcParams['legend.fontsize'] = 14
        plt.rcParams['figure.figsize'] = [8, 6]

        return True


    def _save_data(self, all_data: pd.DataFrame, parameters: dict) -> str:
        # Returns the path where the data were saved.

        # Create folder self.results_folder if it does not exist.
        if not os.path.isdir(self.results_folder):
            os.mkdir(self.results_folder)

        # Create filename.
        thenow = datetime.now()
        filename = thenow.strftime(
            f"{thenow.year:04}{thenow.month:02}{thenow.day:02}_{thenow.hour:02}{thenow.minute:02}{thenow.second:02}")

        # This ensures paths that work in both Unix and Windows.
        filepath = self.results_folder + \
            filename if self.results_folder.endswith(
                '/') else self.results_folder + '/' + filename

        # Save DataFrame to folder.
        all_data.to_csv(RUtils.set_extension(
            filepath + self.analysis_filename_appendix, self.analysis_extension))

        # Save Python script to folder.
        with open(RUtils.set_extension(filepath + self.script_filename_appendix, PJSPlugin.SCRIPT_EXTENSION_BM),
                  "w") as f:
            print(
                f"parameters = {str(parameters)}\n{PJSPlugin.BATCH_MEASURE_SCRIPT}", file=f)

        # Save all open figures.
        for index in plt.get_fignums():
            plt.figure(index)
            plt.savefig(filepath + f"_{index:03}.svg")
            plt.savefig(filepath + f"_{index:03}.png")
            plt.close()

        return filepath

    def _save_notebook(self, parameters: dict, data_path: str) -> bool:
        nb: NotebookNode = self._save_widget_notebook_setup(
            data_path, parameters)
        nb.cells.extend(self._save_widget_notebook_meanplots(parameters).cells)
        nb.cells.extend(self._save_widget_notebook_singleplots().cells)
        nb.cells.extend(self._save_widget_notebook_boxplots().cells)

        nb['metadata'].update({'language_info': {'name': 'python'}})

        fname = RUtils.set_extension(data_path, PyJAMAS.notebook_extension)

        with open(fname, 'w', encoding="utf-8") as f:
            nbf.write(nb, f)

        return True

    def _save_widget_notebook_setup(self, data_path: str, parameters: dict) -> NotebookNode:
        nb: NotebookNode = nbf.v4.new_notebook()
        nb['cells'] = []

        text = f"""# RECOIL VELOCITY notebook {data_path[data_path.rfind(os.sep)+1:]}"""
        nb['cells'].append(nbf.v4.new_markdown_cell(text))

        text = """We start by importing the packages necessary to run and plot the analysis. We also create lists that define what we will be plotting."""
        nb['cells'].append(nbf.v4.new_markdown_cell(text))

        code = f"import ipywidgets as widgets\n" \
               f"import matplotlib\n" \
               f"import matplotlib.pyplot as plt\n" \
               f"%matplotlib inline\n" \
               f"import numpy\n" \
               f"import pandas as pd\n" \
               f"import seaborn as sns"
        nb['cells'].append(nbf.v4.new_code_cell(code))

        code = f"TIME_PLOTS = {self.ABS_MEASUREMENTS}\n" \
               f"BOX_PLOTS = {self.BOX_PLOT_MEASUREMENTS}"
        nb['cells'].append(nbf.v4.new_code_cell(code))

        text = """Run the analysis (uncomment the code in this cell; otherwise see below to load analysis results from disk):"""
        nb['cells'].append(nbf.v4.new_markdown_cell(text))

        # Find plugin index.
        plugin_index = [ind for ind, x in enumerate(self.pjs.plugin_list) if x.name() == self.name()][0]
        
        code = f"# import sys\n" \
               f"# sys.path.extend(['{PyJAMAS.folder.rstrip('/pyjamas')}'])\n" \
               f"# from pyjamas.pjscore import PyJAMAS\n\n" \
               f"# a = PyJAMAS()\n\n" \
               f"# parameters = {str(parameters)}\n\n" \
               f"# a.plugin_list[{plugin_index}].run(parameters)"
        nb['cells'].append(nbf.v4.new_code_cell(code))

        text = """Or load analysis results from disk:"""
        nb['cells'].append(nbf.v4.new_markdown_cell(text))

        code = f"all_data = pd.read_csv('{RUtils.set_extension(data_path + parameters.get('analysis_filename_appendix', PJSPlugin.ANALYSIS_FILENAME_APPENDIX_BM), parameters.get('analysis_extension', PJSPlugin.ANALYSIS_EXTENSION_BM))}')"
        nb['cells'].append(nbf.v4.new_code_cell(code))

        text = """Plot results: first, set up plots."""
        nb['cells'].append(nbf.v4.new_markdown_cell(text))

        code = f"sns.set()\n" \
               f"sns.set_style(\"white\")\n" \
               f"plt.rcParams['font.size'] = 14\n" \
               f"plt.rcParams['font.weight'] = 'bold'\n" \
               f"plt.rcParams['xtick.labelsize'] = 16\n" \
               f"plt.rcParams['ytick.labelsize'] = 16\n" \
               f"plt.rcParams['axes.labelsize'] = 18\n" \
               f"plt.rcParams['axes.labelweight'] = 'bold'\n" \
               f"plt.rcParams['legend.fontsize'] = 14\n" \
               f"plt.rcParams['figure.figsize'] = [8, 6]\n\n" \
               f"n_groups: int = len(numpy.unique(all_data['experimental group']))\n" \
               f"a_palette = sns.color_palette('bright', n_groups)"
        nb['cells'].append(nbf.v4.new_code_cell(code))

        return nb

    def _save_widget_notebook_meanplots(self, parameters: dict) -> NotebookNode:
        nb: NotebookNode = nbf.v4.new_notebook()
        nb['cells'] = []

        text = f"""## Means plots comparing multiple groups.\n""" \
               f"""You may use *plt.savefig("file_name.ext")* after the code that creates a figure to save the figure. *ext* can be png, svg, etc ..."""
        nb['cells'].append(nbf.v4.new_markdown_cell(text))

        code = f"@widgets.interact(metric=TIME_PLOTS)\n" \
               f"def mean_time_plots(metric):\n" \
               f"    ax = sns.lineplot(x='time (s)', y=metric, hue='experimental group', data=all_data, errorbar='se',  err_style='{parameters.get('err_style_value', PJSPlugin.ERR_STYLE_VALUE_BM)}', lw={PJSPlugin.LINE_WIDTH_BM}, palette=a_palette)\n" \
               f"    ax.legend(frameon=False)\n" \
               f"    sns.despine()\n" \
               f"    return"
        nb['cells'].append(nbf.v4.new_code_cell(code))

        return nb

    def _save_widget_notebook_singleplots(self) -> NotebookNode:
        nb: NotebookNode = nbf.v4.new_notebook()
        nb['cells'] = []

        text = f"""## Plot individual curves for each of the experiments.\n""" \
               f"""You may use *plt.savefig("file_name.ext")* after the code that creates a figure to save the figure. *ext* can be png, svg, etc ..."""
        nb['cells'].append(nbf.v4.new_markdown_cell(text))

        code = f"@widgets.interact(metric=TIME_PLOTS)\n" \
               f"def individual_time_plots(metric):\n" \
               f"    ax = sns.lineplot(x='time (s)', y=metric, hue='experimental group', data=all_data, estimator=None, units='experiment index', lw={PJSPlugin.LINE_WIDTH_BM}, palette=a_palette)\n" \
               f"    handles, labels = ax.get_legend_handles_labels()\n" \
               f"    ax.legend(handles=handles[0:], labels=labels[0:], frameon=False)\n" \
               f"    sns.despine()\n" \
               f"    \n" \
               f"    for a_group in {self.group_labels}:\n" \
               f"        plt.figure()\n" \
               f"        thedata = all_data[all_data['experimental group'] == a_group]\n" \
               f"        n_exp: int = len(numpy.unique(thedata['experiment index']).astype(int))\n" \
               f"        ax = sns.lineplot(x='time (s)', y=metric, hue='experiment index', data=thedata, estimator=None, units='experiment index', lw={PJSPlugin.LINE_WIDTH_BM}, palette=sns.color_palette('husl', n_exp))\n" \
               f"        ax.legend([str(x) for x in range(n_exp)], frameon=False, title=a_group)\n" \
               f"        sns.despine()\n" \
               f"    \n" \
               f"    return"
        nb['cells'].append(nbf.v4.new_code_cell(code))

        return nb

    def _save_widget_notebook_boxplots(self) -> NotebookNode:
        nb: NotebookNode = nbf.v4.new_notebook()
        nb['cells'] = []

        text = f"""## Box plots for summary metrics.\n""" \
               f"""You may use *plt.savefig("file_name.ext")* after the code that creates a figure to save the figure. *ext* can be png, svg, etc ..."""
        nb['cells'].append(nbf.v4.new_markdown_cell(text))

        # The rate constant is the same for all the time points of a given image, so we only take the first
        # value per image.
        # sort=False is important to preserve the order of the experimental groups, so that labels in the
        # plots below are correct.
        code = f"thedata = all_data.groupby(['experimental group', 'experiment index'], as_index=False, sort=False).first()"
        nb['cells'].append(nbf.v4.new_code_cell(code))

        code = f"@widgets.interact(metric=BOX_PLOTS)\n" \
               f"def box_plots(metric):\n"
        if self.plot_style_value == 'box':
            code += f"    ax = sns.boxplot(x='experimental group', y=metric, data=thedata, hue='experimental group', dodge=False, whis=100000, palette=a_palette)\n"
        elif self.plot_style_value == 'violin':
            code += f"    ax = sns.violinplot(x='experimental group', y=metric, data=thedata, hue='experimental group', legend='brief', dodge=False)\n"

        code += f"    ax.legend(frameon=False)\n" \
                f"    ax = sns.stripplot(x='experimental group', y=metric, data=thedata, color='k', alpha=0.75, size=6, dodge=False, jitter=0.05)\n" \
                f"    sns.despine()\n" \
                f"    return"
        nb['cells'].append(nbf.v4.new_code_cell(code))

        return nb
